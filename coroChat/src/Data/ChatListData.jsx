import User1 from "../assets/user1.jpeg"
import User2 from '../assets/user2.jpeg';
import User3 from '../assets/user3.jpeg';
import User4 from '../assets/user4.jpeg';
import User5 from '../assets/user5.jpeg';
import User6 from '../assets/user6.jpeg';
import User7 from '../assets/user7.jpeg';
import User8 from '../assets/user8.jpeg';
import User9 from '../assets/user9.jpeg';
import User10 from '../assets/user10.jpeg';
import User11 from '../assets/user11.jpeg';


export const ChatListData = [
  {
    id: 1,
    name: 'tina',
    profile: User1,
    time: '8:12am',
    mute: false,
    massage: 'okkey',
  },
  {
    id: 2,
    name: 'mayank',
    profile: User2,
    time: '8:12am',
    mute: true,
    massage: 'were are you',
  },
  {
    id: 3,
    name: 'ritika',
    profile: User3,
    time: '7:12am',
    mute: false,
      massage: 'no problem',

  },
  {
    id: 4,
    name: 'Anushka',
    profile: User4,
    time: '1:12am',
    mute: false,
    massage: 'send m2 location',
  },
  {
    id: 5,
    name: 'tanay',
    profile:User5,
    time: '5:12pm',
    mute: false,
    massage: 'okkey',
  },
  {
    id: 6,
    name: 'vinod',
    profile: User6,
    time: '8:12am',
    mute: false,
    massage: 'okkey',
  },
  {
    id: 7,
    name: 'priynak',
    profile: User7,
    time: '8:12am',
    mute: false,
    massage: 'okkey',
  },
  {
    id: 8,
    name: 'utsav',
    profile: User8,
    time: '8:12am',
    mute: false,
    massage: 'okkey',
  },
  {
    id: 9,
    name: 'deepak',
    profile: User9,
    time: '8:12am',
    mute: false,
    massage: 'okkey',
  },
  {
    id: 10,
    name: 'reena',
    profile: User10,
    time: '8:12am',
    mute: false,
    massage: 'okkey',
  },
  {
    id: 11,
    name: 'madhri',
    profile: User11,
    time: '8:12am',
    mute: false,
    massage: 'okkey',
  },
];