import CallListScreen from "../Screens/CallListScreen";
import ChatListScreen from "../Screens/ChatListScreen";
import StatusListScreen from "../Screens/StatusListScreen";

export const TabBarData = [
  {
    id: 1,
    route: ChatListScreen,
    name: 'Chat',
  },
  {
    id: 2,
    route: StatusListScreen,
    name: 'Status',
  },
  {
    id: 3,
    route: CallListScreen,
    name: 'Calls',
  },
];