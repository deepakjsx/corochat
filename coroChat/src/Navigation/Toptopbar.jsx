import {StyleSheet} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React from 'react';
import CommunutiyScreen from '../Screens/CommunutiyScreen';
import {Colors} from '../Theme/colors';
import VectorIcon from '../Utals/VectorIcon';
import {TabBarData} from '../Data/TabBarData';

const Tab = createMaterialTopTabNavigator();

const Toptopbar = () => {
  return (
    <Tab.Navigator
      initialRouteName="Chat"
      screenOptions={{
        tabBarActiveTintColor: Colors.tertiary,
        tabBarInactiveTintColor: Colors.white,
        tabBarIndicatorStyle: {
          backgroundColor: Colors.tertiary,
        },
        tabBarStyle: {
          backgroundColor: Colors.primaryColor,
        },
      }}>
      <Tab.Screen
        name="Communutiy"
        component={CommunutiyScreen}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color}) => (
            <VectorIcon
              type="FontAwesome"
              name="users"
              Colors={color}
              size={20}
            />
          ),
        }}
      />
      {TabBarData.map(tab => (
        <Tab.Screen key={tab.id} name={tab.name} component={tab.route} />
      ))}
    </Tab.Navigator>
  );
};

export default Toptopbar;

const styles = StyleSheet.create({});
