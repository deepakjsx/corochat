import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import headerphoto from '../../src/assets/user1.jpeg';
import VectorIcon from '../Utals/VectorIcon';
import { Colors } from '../Theme/colors';
import {useNavigation} from '@react-navigation/native';

const CharHeader = () => {

  const navigation = useNavigation();
  return (
    <>
      <View style={styles.MainContainer}>
        <View style={styles.InnerContainer}>
          <View style={styles.imagetitale}>
            <View style={styles.imagecontainer}>
              <VectorIcon
                name="arrow-back"
                type="Ionicons"
                size={25}
                color={Colors.white}
                onPress={() => navigation.goBack()}
              />
              <Image source={headerphoto} size={25} style={styles.Image} />
              <Text style={styles.nameTitle}>Tina</Text>
            </View>
            <View style={styles.icons}>
              <VectorIcon
                type="Ionicons"
                name="videocam"
                color={Colors.white}
                size={25}
              />
              <VectorIcon
                type="Ionicons"
                name="search"
                color={Colors.white}
                size={25}
              />
              <VectorIcon
                type="Entypo"
                name="dots-three-vertical"
                color={Colors.white}
                size={20}
              />
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

export default CharHeader;

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Colors.primaryColor,
  },
  InnerContainer: {
    justifyContent: 'space-between',
  },
  imagetitale: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    height: 70,
    justifyContent: 'space-between',
  },
  Image: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  nameTitle: {
    fontSize: 17,
    color: Colors.white,
    marginLeft: 10,
  },

  imagecontainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icons: {
    flexDirection: 'row',
    marginHorizontal: 15,
    paddingLeft:5
  },
});
