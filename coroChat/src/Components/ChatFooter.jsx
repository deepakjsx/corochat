import {Alert, StyleSheet, Text, TextInput, View} from 'react-native';
import React, {useState} from 'react';
import VectorIcon from '../Utals/VectorIcon';
import {Colors} from '../Theme/colors';

const chatFooter = () => {
  const [massage, setMassage] = useState('');
  const [sendEnable, setsendEnable] = useState(false);

  const onChange = value => {
    setMassage(value);
    setsendEnable(true);
  };
  const onSend = () => {
    setMassage('');
    setsendEnable(false);
    Alert.alert('massage has been send');
  };

  return (
    <>
      <View style={styles.MainContainer}>
        <View style={styles.LeftContainer}>
          <View style={styles.leftIcons}>
            <VectorIcon
              name="emoji-emotions"
              type="MaterialIcons"
              size={20}
              color={Colors.white}
            />
            <TextInput
              placeholder="Massage"
              placeholderTextColor={Colors.textGrey}
              style={styles.TextInput}
              onChange={value => onChange(value)}
              value={massage}
            />
          </View>
          <View style={styles.rightIcons}>
            <VectorIcon
              name="attachment"
              type="Entypo"
              size={18}
              color={Colors.white}
            />
            {!sendEnable && (
              <>
                <VectorIcon
                  name="rupee"
                  type="FontAwesome"
                  size={18}
                  color={Colors.white}
                />
                <VectorIcon
                  name="camera"
                  type="FontAwesome"
                  size={18}
                  color={Colors.white}
                />
              </>
            )}
          </View>
        </View>
        <View style={styles.RightContainer}>
          {sendEnable ? (
            <VectorIcon
              name="send"
              type="MaterialCommunityIcons"
              size={25}
              color={Colors.white}
              onPress={onSend}
            />
          ) : (
            <VectorIcon
              name="microphone"
              type="MaterialCommunityIcons"
              size={25}
              color={Colors.white}
            />
          )}
        </View>
      </View>
    </>
  );
};

export default chatFooter;

const styles = StyleSheet.create({
  MainContainer: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: Colors.black,
    paddingVertical: 6,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    
    width: '100%',
    
  },
  TextInput: {
    color: Colors.white,
    fontSize: 17,
    marginLeft: 5,
  },
  LeftContainer: {
    width: '85%',
    flexDirection: 'row',
    backgroundColor: Colors.primaryColor,
    borderRadius: 30,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 2,
    gap: 10,
  },
  RightContainer: {
    borderColor: 'green',
    padding: 7,
    backgroundColor: Colors.teal,
    borderRadius: 50,
  },
});
