import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import React from 'react'
import { Colors } from '../Theme/colors'
import VectorIcon from '../Utals/VectorIcon'
import { ChatListData } from '../Data/ChatListData';
import { useNavigation } from '@react-navigation/native';
const ChatList = () => {
  const navigation = useNavigation()
  const onNavigate = () => {
    navigation.navigate('ChatScreen');
  }
    return (
      <>
        {ChatListData.map(item => (
          <View key={item.id}>
            <TouchableOpacity style={styles.Container} onPress={onNavigate}>
              <View style={styles.TextContainer}>
                <Image source={item.profile} style={styles.profileImage} />
                <View>
                  <Text style={styles.username1}>{item.name}</Text>
                  <Text style={styles.Massage}>{item.massage}</Text>
                </View>
              </View>
              <View>
                <Text style={styles.timeset}>9:45am</Text>
                <VectorIcon
                  type="MaterialCommunityIcons"
                  name="volume-variant-off"
                  size={22}
                  color={Colors.textGrey}
                  style={styles.muteicon}
                />
              </View>
            </TouchableOpacity>
          </View>
        ))}
      </>
    );
}

export default ChatList

const styles = StyleSheet.create({
    profileImage: {
        borderRadius:50,
        height: 40,
        width: 40,
        marginRight:25
    },
    Container: {
        backgroundColor: Colors.background,
        padding: 8,
        flexDirection:"row",
        justifyContent:"space-between"
    },
    username1: {
        color: Colors.textColor,
        fontSize:16
    },
    Massage: {
        color: Colors.textGrey,
        fontSize: 14,
        marginTop:5
    },
    TextContainer: {
        flexDirection:"row",
    },
    timeset: {
        color: Colors.textGrey,
        fontSize:12,
    },
    muteicon: {
        marginTop:5
    }
})