import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Colors} from '../Theme/colors';
import VectorIcon from '../Utals/VectorIcon';
import { massageData } from '../Data/massageData';

const ChatBody = () => {
  const UserMassageView = (massage,time) => (
    <View style={styles.userContainer}>
      <View style={styles.userinnerContainer}>
        <Text style={styles.massage}>{ massage}</Text>
        <Text style={styles.time}>{time}</Text>
        <VectorIcon
          style={styles.cheak}
          name="check-double"
          type="FontAwesome5"
          color={Colors.blue}
          size={11}
        />
      </View>
    </View>
  );

  const OtherMassageView = (massage,time) => (
    <View style={styles.otherContainer}>
      <Text style={styles.massage}>hii</Text>
      <Text style={styles.time}>9:45am</Text>
    </View> 
  );

  return (
    <View>
      {massageData.map(item => (
        <>
          <UserMassageView massage={item.massage} time={item.time} />
          <OtherMassageView massage={item.massage} time={item.time} />
        </>
      ))}
    </View>
  );
};

export default ChatBody;

const styles = StyleSheet.create({
  userContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    justifyContent: 'flex-end',
  },
  otherContainer: {
    flexDirection: 'row',
    backgroundColor: Colors.primaryColor,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderBottomLeftRadius: 30,
    width:120
  },
  userinnerContainer: {
    flexDirection: 'row',
    backgroundColor: Colors.teal,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderTopLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    
  },
  time: {
    padding: 5,
    color: Colors.white,
    fontSize: 13,
  },
  massage: {
    padding: 5,
    color: Colors.white,
    fontSize: 13,
  },
  cheak: {
    justifyContent: 'center',
  },
});
