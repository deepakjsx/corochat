import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Whatsapplogo from '../assets/corochatwhite.png';
import {Colors} from '../Theme/colors';
import VectorIcon from '../Utals/VectorIcon';

const Header = () => {
  return (
    <View style={styles.MainContainer}>
      <Image source={Whatsapplogo} style={styles.logostyle} />
      <View style={styles.headerIcons}>
        <VectorIcon
          type="Feather"
          name="camera"
          color={Colors.white} // Update color prop
          size={20}
        />
        <VectorIcon
          type="Ionicons"
          name="search"
          color={Colors.white} // Update color prop
          size={20}
          style={styles.iconstyle}
        />
        <VectorIcon
          type="Entypo"
          name="dots-three-vertical"
          color={Colors.white} // Update color prop
          size={20}
        />
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  logostyle: {
    height: 30,
    width: 120,
  },
  MainContainer: {
    backgroundColor: Colors.primaryColor,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerIcons: {
    flexDirection: 'row',
  },
  iconstyle: {
    marginHorizontal:15
  }
});
