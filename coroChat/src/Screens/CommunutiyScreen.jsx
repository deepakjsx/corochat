import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import communicatyImage from '../assets/community-img.png';
import { Colors } from '../Theme/colors';
const CommunutiyScreen = () => {
  return (
    <View style={styles.mainImageContainer}>
      <Image style={styles.communityImage} source={communicatyImage} />
      <Text style={styles.communitytext}>Stay Connected with communities</Text>
      <Text style={styles.subText}>
        Easily organize your related groups and send announcements. Now, your
        communities, like neighbourhood or schools, can have their own space.
      </Text>
    </View>
  );
}

export default CommunutiyScreen

const styles = StyleSheet.create({
  communityImage: {
    height: 150,
    width: 250,
  },
  mainImageContainer: {
    backgroundColor: Colors.background,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  communitytext: {
    color: Colors.white,
    fontSize: 20,
    fontWeight: '500',
    marginTop: 40,
  },
  subText: {
    color: Colors.textGrey,
    fontSize: 16,
    textAlign: "center",
    paddingHorizontal: 30,
    marginTop: 5,
    lineHeight: 20,
    letterSpacing:0.6
  }
});