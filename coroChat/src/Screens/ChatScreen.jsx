import {ImageBackground, StyleSheet, View} from 'react-native';
import React from 'react';
import CharHeader from '../Components/CharHeader';
import ChatBody from '../Components/ChatBody';
import ChatFooter from '../Components/ChatFooter';
import wallpaper from '../assets/wallpaper.jpeg';

const ChatScreen = () => {
  return (
    <View style={styles.container}>
      <CharHeader />
      <ImageBackground source={wallpaper} style={styles.wallpaper}>
        <ChatBody />
      </ImageBackground>
      <ChatFooter />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  wallpaper: {
    flex: 1,
    padding:12
  },
});

export default ChatScreen;
