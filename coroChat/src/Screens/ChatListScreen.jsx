import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import ChatList from '../Components/ChatList'
import VectorIcon from '../Utals/VectorIcon'
import { Colors } from '../Theme/colors'

const ChatListScreen = () => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <ChatList />
      </ScrollView>
      <TouchableOpacity style={styles.contatIcon}>
        <VectorIcon
          type="MaterialCommunityIcons"
          name="message-reply-text"
          size={14}
          color={Colors.white}
        />
      </TouchableOpacity>
    </View>
  );
}

export default ChatListScreen

const styles = StyleSheet.create({
  
  contatIcon: {
    backgroundColor: Colors.tertiary,
    height: 50,
    width: 50,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: 20,
    right:20
  },
  container: {
    position: "relative",
    flex: 1,
    backgroundColor:Colors.background
  }
})