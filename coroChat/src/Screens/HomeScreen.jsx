import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../Components/Header'
import Toptopbar from '../Navigation/Toptopbar'

const HomeScreen = () => {
  return (
    <>
      <Header />
      <Toptopbar/>
    </>
  )
}

export default HomeScreen

const styles = StyleSheet.create({})